export interface IData {
    agent: string;
    country: string;
    address: string;
    date: Date;
    pitagoras: number;
}
