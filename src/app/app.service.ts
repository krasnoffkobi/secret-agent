import { Injectable } from '@angular/core';
import { IData } from './IData';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  getData(): Promise<IData[]> {
    return this.http.get<IData[]>('/assets/json/data.json').toPromise();
  }

  getGeoLocation(address: string): Promise<any> {
    // tslint:disable-next-line: max-line-length
    return this.http.get<IData[]>(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURI(address)}&key=AIzaSyAHVXzCCk1_ENFka_ahOxxnyX5JV0FK1oE`).toPromise();
  }
}
