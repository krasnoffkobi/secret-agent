import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { IDistinctAgents } from './IDistinctAgents';
import { IDistinctCountries } from './idistinctCountries';
import { IData } from './IData';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'tikalCodeChallenge';

  selectedCountry: string;
  rows: IData[] = [];
  minPitagoras: number;
  maxPitagoras: number;

  constructor(public appService: AppService) {}

  ngOnInit(): void {
    this.init();
  }

  async init() {
    this.rows = await this.appService.getData();
    this.rows.forEach(el => {
      el.pitagoras = 0;
    });

    this.getIsolatedCountry();

    this.getGeoLocation();
  }

  private async getGeoLocation() {
    const downingCoords = await this.appService.getGeoLocation('10 Downing st. London');
    console.log('downingCoords', downingCoords);
    console.log('downingCoords', downingCoords.results[0].geometry.location.lat, downingCoords.results[0].geometry.location.lng);

    for (let i = 0; i < this.rows.length; i++) {
      if (this.rows[i].address) {
        const coords = await this.appService.getGeoLocation(this.rows[i].address);
        const pitagoras = Math.pow(downingCoords.results[0].geometry.location.lat - coords.results[0].geometry.location.lat, 2) +
        Math.pow(downingCoords.results[0].geometry.location.lng - coords.results[0].geometry.location.lng, 2);
        this.rows[i].pitagoras = pitagoras;
      }
    }

    const pitagorasList = this.rows.map(el => el.pitagoras);

    pitagorasList.sort((a, b) => {
      return a - b;
    });

    this.minPitagoras = pitagorasList[0];
    this.maxPitagoras = pitagorasList[pitagorasList.length - 1];
  }

  private getIsolatedCountry() {
    // get distinct agents and their number of missions
    let distinctAgentsNum = this.rows.map(el => el.agent);

    distinctAgentsNum = distinctAgentsNum.filter((value, index, self) => {
      return self.indexOf(value) === index;
    });

    const distinctAgentsList: IDistinctAgents[] = [];

    distinctAgentsNum.forEach(el => {
      const newElement: IDistinctAgents = {
        angentId: el,
        missionCount: this.rows.filter(dataElement => dataElement.agent === el).length
      };
      distinctAgentsList.push(newElement);
    });

    console.log('distinctAgentsList', distinctAgentsList);

    // get distinct countries and their number of missions
    let distinctCountriesUnique = this.rows.map(el => el.country);

    distinctCountriesUnique = distinctCountriesUnique.filter((value, index, self) => {
      return self.indexOf(value) === index;
    });

    const distinctCountriesList: IDistinctCountries[] = [];

    distinctCountriesUnique.forEach(el => {
      const newElement: IDistinctCountries = {
        CountryName: el,
        missionCount: this.rows.filter(dataElement => dataElement.country === el).filter(dataElement => {
          const agentDetails = distinctAgentsList.find(det => det.missionCount === 1 && dataElement.agent === det.angentId);
          return agentDetails ? true : false;
        }).length
      };
      distinctCountriesList.push(newElement);
    });

    distinctCountriesList.sort((a, b) => {
      return b.missionCount - a.missionCount;
    });

    this.selectedCountry = distinctCountriesList[0].CountryName;

    console.log(distinctCountriesList);
  }
}
